  


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>mobileGmap/jquery.mobilegmap.js at master · joggink/mobileGmap</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="http://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="/mMpdkCPfwSTBdEdCnAc4U1V5TUtMf1jRMkvsiGqxpI=" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/assets/github-8c237cc402e3d4bc024750691ccce4fd5eddee2e.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://a248.e.akamai.net/assets.github.com/assets/github2-9b6eed4e9a41cc2dd85690cc19abdc063bd87d9a.css" media="all" rel="stylesheet" type="text/css" />
    


      <script src="https://a248.e.akamai.net/assets.github.com/assets/frameworks-d76b58e749b52bc47a4c46620bf2c320fabe5248.js" type="text/javascript"></script>
      <script src="https://a248.e.akamai.net/assets.github.com/assets/github-80a3b7f6948ed4122df090b89e975d21a9c88297.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="581859cfff990c09f7c9d19575e3e6f4">

        <link data-pjax-transient rel='permalink' href='/joggink/mobileGmap/blob/fb05b27d69cd6860a5f5e8f7526708de5f63d20a/jquery.mobilegmap.js'>
    <meta property="og:title" content="mobileGmap"/>
    <meta property="og:type" content="githubog:gitrepository"/>
    <meta property="og:url" content="https://github.com/joggink/mobileGmap"/>
    <meta property="og:image" content="https://secure.gravatar.com/avatar/35a223d0857b71725762298f9ca404d7?s=420&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png"/>
    <meta property="og:site_name" content="GitHub"/>
    <meta property="og:description" content="Responsive google maps. Contribute to mobileGmap development by creating an account on GitHub."/>
    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:site" content="@GitHub">
    <meta property="twitter:title" content="joggink/mobileGmap"/>

    <meta name="description" content="Responsive google maps. Contribute to mobileGmap development by creating an account on GitHub." />

  <link href="https://github.com/joggink/mobileGmap/commits/master.atom" rel="alternate" title="Recent Commits to mobileGmap:master" type="application/atom+xml" />

  </head>


  <body class="logged_in page-blob linux vis-public env-production  ">
    <div id="wrapper">

      

      

      

      


        <div class="header header-logged-in true">
          <div class="container clearfix">

            <a class="header-logo-blacktocat" href="https://github.com/">
  <span class="mega-icon mega-icon-blacktocat"></span>
</a>

            <div class="divider-vertical"></div>

              <a href="/notifications" class="notification-indicator tooltipped downwards" title="You have no unread notifications">
    <span class="mail-status all-read"></span>
  </a>
  <div class="divider-vertical"></div>


              <div class="command-bar js-command-bar  ">
      <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">
  <a href="/search/advanced" class="advanced-search-icon tooltipped downwards command-bar-search" id="advanced_search" title="Advanced search"><span class="mini-icon mini-icon-advanced-search "></span></a>

  <input type="text" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" data-username="florescu" autocapitalize="off">

  <span class="mini-icon help tooltipped downwards" title="Show command bar help">
    <span class="mini-icon mini-icon-help"></span>
  </span>

  <input type="hidden" name="ref" value="commandbar">

  <div class="divider-vertical"></div>
</form>
  <ul class="top-nav">
      <li class="explore"><a href="https://github.com/explore">Explore</a></li>
      <li><a href="https://gist.github.com">Gist</a></li>
      <li><a href="/blog">Blog</a></li>
    <li><a href="http://help.github.com">Help</a></li>
  </ul>
</div>


            

  
    <ul id="user-links">
      <li>
        <a href="https://github.com/florescu" class="name">
          <img height="20" src="https://secure.gravatar.com/avatar/01d6359ad83274730ec1d3d36be9b181?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /> florescu
        </a>
      </li>
      <li>
        <a href="/new" id="new_repo" class="tooltipped downwards" title="Create a new repo">
          <span class="mini-icon mini-icon-create"></span>
        </a>
      </li>
      <li>
        <a href="/settings/profile" id="account_settings"
          class="tooltipped downwards"
          title="Account settings ">
          <span class="mini-icon mini-icon-account-settings"></span>
        </a>
      </li>
      <li>
        <a href="/logout" data-method="post" id="logout" class="tooltipped downwards" title="Sign out">
          <span class="mini-icon mini-icon-logout"></span>
        </a>
      </li>
    </ul>



            
          </div>
        </div>


      

      


            <div class="site hfeed" itemscope itemtype="http://schema.org/WebPage">
      <div class="hentry">
        
        <div class="pagehead repohead instapaper_ignore readability-menu ">
          <div class="container">
            <div class="title-actions-bar">
              


<ul class="pagehead-actions">


    <li class="subscription">
      <form accept-charset="UTF-8" action="/notifications/subscribe" data-autosubmit="true" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="authenticity_token" type="hidden" value="/mMpdkCPfwSTBdEdCnAc4U1V5TUtMf1jRMkvsiGqxpI=" /></div>  <input id="repository_id" name="repository_id" type="hidden" value="3090767" />

    <div class="select-menu js-menu-container js-select-menu">
      <span class="minibutton select-menu-button js-menu-target">
        <span class="js-select-button">
          <span class="mini-icon mini-icon-watching"></span>
          Watch
        </span>
      </span>

      <div class="select-menu-modal-holder js-menu-content">
        <div class="select-menu-modal">
          <div class="select-menu-header">
            <span class="select-menu-title">Notification status</span>
            <span class="mini-icon mini-icon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-list js-navigation-container">

            <div class="select-menu-item js-navigation-item js-navigation-target selected">
              <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
              <div class="select-menu-item-text">
                <input checked="checked" id="do_included" name="do" type="radio" value="included" />
                <h4>Not watching</h4>
                <span class="description">You only receive notifications for discussions in which you participate or are @mentioned.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="mini-icon mini-icon-watching"></span>
                  Watch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item js-navigation-target ">
              <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
              <div class="select-menu-item-text">
                <input id="do_subscribed" name="do" type="radio" value="subscribed" />
                <h4>Watching</h4>
                <span class="description">You receive notifications for all discussions in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="mini-icon mini-icon-unwatch"></span>
                  Unwatch
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

            <div class="select-menu-item js-navigation-item js-navigation-target ">
              <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
              <div class="select-menu-item-text">
                <input id="do_ignore" name="do" type="radio" value="ignore" />
                <h4>Ignoring</h4>
                <span class="description">You do not receive any notifications for discussions in this repository.</span>
                <span class="js-select-button-text hidden-select-button-text">
                  <span class="mini-icon mini-icon-mute"></span>
                  Stop ignoring
                </span>
              </div>
            </div> <!-- /.select-menu-item -->

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

</form>
    </li>

    <li class="js-toggler-container js-social-container starring-container ">
      <a href="/joggink/mobileGmap/unstar" class="minibutton js-toggler-target star-button starred upwards" title="Unstar this repo" data-remote="true" data-method="post" rel="nofollow">
        <span class="mini-icon mini-icon-remove-star"></span>
        <span class="text">Unstar</span>
      </a>
      <a href="/joggink/mobileGmap/star" class="minibutton js-toggler-target star-button unstarred upwards" title="Star this repo" data-remote="true" data-method="post" rel="nofollow">
        <span class="mini-icon mini-icon-star"></span>
        <span class="text">Star</span>
      </a>
      <a class="social-count js-social-count" href="/joggink/mobileGmap/stargazers">79</a>
    </li>

        <li>
          <a href="/joggink/mobileGmap/fork_select" class="minibutton js-toggler-target fork-button lighter upwards" title="Fork this repo" rel="facebox nofollow">
            <span class="mini-icon mini-icon-branch-create"></span>
            <span class="text">Fork</span>
          </a>
          <a href="/joggink/mobileGmap/network" class="social-count">28</a>
        </li>


</ul>

              <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
                <span class="repo-label"><span>public</span></span>
                <span class="mega-icon mega-icon-public-repo"></span>
                <span class="author vcard">
                  <a href="/joggink" class="url fn" itemprop="url" rel="author">
                  <span itemprop="title">joggink</span>
                  </a></span> /
                <strong><a href="/joggink/mobileGmap" class="js-current-repository">mobileGmap</a></strong>
              </h1>
            </div>

            
  <ul class="tabs">
    <li><a href="/joggink/mobileGmap" class="selected" highlight="repo_source repo_downloads repo_commits repo_tags repo_branches">Code</a></li>
    <li><a href="/joggink/mobileGmap/network" highlight="repo_network">Network</a></li>
    <li><a href="/joggink/mobileGmap/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>2</span></a></li>

      <li><a href="/joggink/mobileGmap/issues" highlight="repo_issues">Issues <span class='counter'>2</span></a></li>

      <li><a href="/joggink/mobileGmap/wiki" highlight="repo_wiki">Wiki</a></li>


    <li><a href="/joggink/mobileGmap/graphs" highlight="repo_graphs repo_contributors">Graphs</a></li>


  </ul>
  
<div class="tabnav">

  <span class="tabnav-right">
    <ul class="tabnav-tabs">
          <li><a href="/joggink/mobileGmap/tags" class="tabnav-tab" highlight="repo_tags">Tags <span class="counter blank">0</span></a></li>
    </ul>
    
  </span>

  <div class="tabnav-widget scope">


    <div class="select-menu js-menu-container js-select-menu js-branch-menu">
      <a class="minibutton select-menu-button js-menu-target" data-hotkey="w" data-ref="master">
        <span class="mini-icon mini-icon-branch"></span>
        <i>branch:</i>
        <span class="js-select-button">master</span>
      </a>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container">

        <div class="select-menu-modal">
          <div class="select-menu-header">
            <span class="select-menu-title">Switch branches/tags</span>
            <span class="mini-icon mini-icon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-filters">
            <div class="select-menu-text-filter">
              <input type="text" id="commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
            </div>
            <div class="select-menu-tabs">
              <ul>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
                </li>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
                </li>
              </ul>
            </div><!-- /.select-menu-tabs -->
          </div><!-- /.select-menu-filters -->

          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="branches">

            <div data-filterable-for="commitish-filter-field" data-filterable-type="substring">

                <div class="select-menu-item js-navigation-item js-navigation-target selected">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/joggink/mobileGmap/blob/master/jquery.mobilegmap.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="master" rel="nofollow" title="master">master</a>
                </div> <!-- /.select-menu-item -->
            </div>

              <div class="select-menu-no-results">Nothing to show</div>
          </div> <!-- /.select-menu-list -->


          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="tags">
            <div data-filterable-for="commitish-filter-field" data-filterable-type="substring">

            </div>

            <div class="select-menu-no-results">Nothing to show</div>

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

  </div> <!-- /.scope -->

  <ul class="tabnav-tabs">
    <li><a href="/joggink/mobileGmap" class="selected tabnav-tab" highlight="repo_source">Files</a></li>
    <li><a href="/joggink/mobileGmap/commits/master" class="tabnav-tab" highlight="repo_commits">Commits</a></li>
    <li><a href="/joggink/mobileGmap/branches" class="tabnav-tab" highlight="repo_branches" rel="nofollow">Branches <span class="counter ">1</span></a></li>
  </ul>

</div>

  
  
  


            
          </div>
        </div><!-- /.repohead -->

        <div id="js-repo-pjax-container" class="container context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:2787e7d4200c839292d902fa9da30c36 -->
<!-- blob contrib frag key: views10/v8/blob_contributors:v21:2787e7d4200c839292d902fa9da30c36 -->


<div id="slider">
    <div class="frame-meta">

      <p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

        <div class="breadcrumb">
          <span class='bold'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/joggink/mobileGmap" class="js-slide-to" data-branch="master" data-direction="back" itemscope="url"><span itemprop="title">mobileGmap</span></a></span></span><span class="separator"> / </span><strong class="final-path">jquery.mobilegmap.js</strong> <span class="js-zeroclipboard zeroclipboard-button" data-clipboard-text="jquery.mobilegmap.js" data-copied-hint="copied!" title="copy to clipboard"><span class="mini-icon mini-icon-clipboard"></span></span>
        </div>

      <a href="/joggink/mobileGmap/find/master" class="js-slide-to" data-hotkey="t" style="display:none">Show File Finder</a>


        
  <div class="commit file-history-tease">
    <img class="main-avatar" height="24" src="https://secure.gravatar.com/avatar/554cf2014df70e8e0fbbcfbf7d1be3a0?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
    <span class="author"><a href="/detj" rel="author">detj</a></span>
    <time class="js-relative-date" datetime="2012-02-03T08:01:38-08:00" title="2012-02-03 08:01:38">February 03, 2012</time>
    <div class="commit-title">
        <a href="/joggink/mobileGmap/commit/8f5d1efdf4edaef51ec860bead2dc4672ebb6a79" class="message">Added a radix to parseInt()</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>2</strong> contributors</a></p>
          <a class="avatar tooltipped downwards" title="joggink" href="/joggink/mobileGmap/commits/master/jquery.mobilegmap.js?author=joggink"><img height="20" src="https://secure.gravatar.com/avatar/35a223d0857b71725762298f9ca404d7?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="detj" href="/joggink/mobileGmap/commits/master/jquery.mobilegmap.js?author=detj"><img height="20" src="https://secure.gravatar.com/avatar/554cf2014df70e8e0fbbcfbf7d1be3a0?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2>Users on GitHub who have contributed to this file</h2>
      <ul class="facebox-user-list">
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/35a223d0857b71725762298f9ca404d7?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/joggink">joggink</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/554cf2014df70e8e0fbbcfbf7d1be3a0?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/detj">detj</a>
        </li>
      </ul>
    </div>
  </div>


    </div><!-- ./.frame-meta -->

    <div class="frames">
      <div class="frame" data-permalink-url="/joggink/mobileGmap/blob/fb05b27d69cd6860a5f5e8f7526708de5f63d20a/jquery.mobilegmap.js" data-title="mobileGmap/jquery.mobilegmap.js at master · joggink/mobileGmap · GitHub" data-type="blob">

        <div id="files" class="bubble">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><b class="mini-icon mini-icon-text-file"></b></span>
                <span class="mode" title="File Mode">file</span>
                  <span>129 lines (118 sloc)</span>
                <span>4.269 kb</span>
              </div>
              <div class="actions">
                <div class="button-group">
                        <a class="minibutton tooltipped leftwards"
                           title="Clicking this button will automatically fork this project so you can edit the file"
                           href="/joggink/mobileGmap/edit/master/jquery.mobilegmap.js"
                           data-method="post" rel="nofollow">Edit</a>
                  <a href="/joggink/mobileGmap/raw/master/jquery.mobilegmap.js" class="button minibutton " id="raw-url">Raw</a>
                    <a href="/joggink/mobileGmap/blame/master/jquery.mobilegmap.js" class="button minibutton ">Blame</a>
                  <a href="/joggink/mobileGmap/commits/master/jquery.mobilegmap.js" class="button minibutton " rel="nofollow">History</a>
                </div><!-- /.button-group -->
              </div><!-- /.actions -->

            </div>
                <div class="data type-javascript js-blob-data">
      <table cellpadding="0" cellspacing="0" class="lines">
        <tr>
          <td>
            <pre class="line_numbers"><span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
<span id="L97" rel="#L97">97</span>
<span id="L98" rel="#L98">98</span>
<span id="L99" rel="#L99">99</span>
<span id="L100" rel="#L100">100</span>
<span id="L101" rel="#L101">101</span>
<span id="L102" rel="#L102">102</span>
<span id="L103" rel="#L103">103</span>
<span id="L104" rel="#L104">104</span>
<span id="L105" rel="#L105">105</span>
<span id="L106" rel="#L106">106</span>
<span id="L107" rel="#L107">107</span>
<span id="L108" rel="#L108">108</span>
<span id="L109" rel="#L109">109</span>
<span id="L110" rel="#L110">110</span>
<span id="L111" rel="#L111">111</span>
<span id="L112" rel="#L112">112</span>
<span id="L113" rel="#L113">113</span>
<span id="L114" rel="#L114">114</span>
<span id="L115" rel="#L115">115</span>
<span id="L116" rel="#L116">116</span>
<span id="L117" rel="#L117">117</span>
<span id="L118" rel="#L118">118</span>
<span id="L119" rel="#L119">119</span>
<span id="L120" rel="#L120">120</span>
<span id="L121" rel="#L121">121</span>
<span id="L122" rel="#L122">122</span>
<span id="L123" rel="#L123">123</span>
<span id="L124" rel="#L124">124</span>
<span id="L125" rel="#L125">125</span>
<span id="L126" rel="#L126">126</span>
<span id="L127" rel="#L127">127</span>
<span id="L128" rel="#L128">128</span>
</pre>
          </td>
          <td width="100%">
                  <div class="highlight"><pre><div class='line' id='LC1'><span class="cm">/**</span></div><div class='line' id='LC2'><span class="cm"> * jQuery Mobile Google maps</span></div><div class='line' id='LC3'><span class="cm"> * @Author: Jochen Vandendriessche &lt;jochen@builtbyrobot.com&gt;</span></div><div class='line' id='LC4'><span class="cm"> * @Author URI: http://builtbyrobot.com</span></div><div class='line' id='LC5'><span class="cm"> *</span></div><div class='line' id='LC6'><span class="cm"> * @TODO:</span></div><div class='line' id='LC7'><span class="cm"> * - fix https image requests</span></div><div class='line' id='LC8'><span class="cm">**/</span></div><div class='line' id='LC9'><br/></div><div class='line' id='LC10'><span class="p">(</span><span class="kd">function</span><span class="p">(</span><span class="nx">$</span><span class="p">){</span></div><div class='line' id='LC11'>	<span class="s2">&quot;use strict&quot;</span><span class="p">;</span></div><div class='line' id='LC12'><br/></div><div class='line' id='LC13'>	<span class="kd">var</span> <span class="nx">methods</span> <span class="o">=</span> <span class="p">{</span></div><div class='line' id='LC14'>		<span class="nx">init</span> <span class="o">:</span> <span class="kd">function</span><span class="p">(</span><span class="nx">config</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC15'>			<span class="kd">var</span> <span class="nx">options</span> <span class="o">=</span> <span class="nx">$</span><span class="p">.</span><span class="nx">extend</span><span class="p">({</span></div><div class='line' id='LC16'>				<span class="nx">deviceWidth</span><span class="o">:</span> <span class="mi">480</span><span class="p">,</span></div><div class='line' id='LC17'>				<span class="nx">showMarker</span><span class="o">:</span> <span class="kc">true</span><span class="p">,</span></div><div class='line' id='LC18'>			<span class="p">},</span> <span class="nx">config</span><span class="p">),</span></div><div class='line' id='LC19'>			<span class="nx">settings</span> <span class="o">=</span> <span class="p">{},</span></div><div class='line' id='LC20'>			<span class="nx">markers</span> <span class="o">=</span> <span class="p">[];</span></div><div class='line' id='LC21'>			<span class="c1">// we&#39;ll use the width of the device, because we stopped browsersniffing</span></div><div class='line' id='LC22'>			<span class="c1">// a long time ago. Anyway, we want to target _every_ small display</span></div><div class='line' id='LC23'>			<span class="kd">var</span> <span class="nx">_o</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">);</span> <span class="c1">// store the jqyuery object once</span></div><div class='line' id='LC24'>			<span class="c1">// iframe?</span></div><div class='line' id='LC25'>			<span class="c1">//&lt;iframe width=&quot;425&quot; height=&quot;350&quot; frameborder=&quot;0&quot; scrolling=&quot;no&quot; marginheight=&quot;0&quot; marginwidth=&quot;0&quot; src=&quot;http://maps.google.be/maps?f=q&amp;amp;source=s_q&amp;amp;hl=nl&amp;amp;geocode=&amp;amp;q=Brugse+Heirweg+37,+aartrijke&amp;amp;aq=&amp;amp;sll=51.122175,3.086483&amp;amp;sspn=0.009253,0.021651&amp;amp;vpsrc=0&amp;amp;ie=UTF8&amp;amp;hq=&amp;amp;hnear=Brugse+Heirweg+37,+8211+Zedelgem,+West-Vlaanderen,+Vlaams+Gewest&amp;amp;t=m&amp;amp;z=14&amp;amp;ll=51.122175,3.086483&amp;amp;output=embed&quot;&gt;&lt;/iframe&gt;</span></div><div class='line' id='LC26'>			<span class="nx">options</span><span class="p">.</span><span class="nx">imgURI</span> <span class="o">=</span> <span class="s1">&#39;http://maps.googleapis.com/maps/api/staticmap?&#39;</span><span class="p">;</span></div><div class='line' id='LC27'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">center</span> <span class="o">=</span> <span class="s1">&#39;Brussels Belgium&#39;</span><span class="p">;</span></div><div class='line' id='LC28'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">zoom</span> <span class="o">=</span> <span class="s1">&#39;5&#39;</span><span class="p">;</span></div><div class='line' id='LC29'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">size</span> <span class="o">=</span> <span class="nx">screen</span><span class="p">.</span><span class="nx">width</span> <span class="o">+</span> <span class="s1">&#39;x&#39;</span> <span class="o">+</span>  <span class="mi">480</span><span class="p">;</span></div><div class='line' id='LC30'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">scale</span> <span class="o">=</span> <span class="nb">window</span><span class="p">.</span><span class="nx">devicePixelRatio</span> <span class="o">?</span> <span class="nb">window</span><span class="p">.</span><span class="nx">devicePixelRatio</span> <span class="o">:</span> <span class="mi">1</span><span class="p">;</span></div><div class='line' id='LC31'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">maptype</span> <span class="o">=</span> <span class="s1">&#39;roadmap&#39;</span><span class="p">;</span></div><div class='line' id='LC32'>			<span class="nx">settings</span><span class="p">.</span><span class="nx">sensor</span> <span class="o">=</span> <span class="kc">false</span><span class="p">;</span></div><div class='line' id='LC33'>			<span class="nx">options</span><span class="p">.</span><span class="nx">settings</span> <span class="o">=</span> <span class="nx">settings</span><span class="p">;</span></div><div class='line' id='LC34'><br/></div><div class='line' id='LC35'>			<span class="k">if</span> <span class="p">(</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-center&#39;</span><span class="p">)){</span></div><div class='line' id='LC36'>				<span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">center</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-center&#39;</span><span class="p">).</span><span class="nx">replace</span><span class="p">(</span><span class="sr">/ /gi</span><span class="p">,</span> <span class="s1">&#39;+&#39;</span><span class="p">);</span></div><div class='line' id='LC37'>			<span class="p">}</span></div><div class='line' id='LC38'>			<span class="k">if</span> <span class="p">(</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-zoom&#39;</span><span class="p">)){</span></div><div class='line' id='LC39'>				<span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">zoom</span> <span class="o">=</span> <span class="nb">parseInt</span><span class="p">(</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-zoom&#39;</span><span class="p">));</span></div><div class='line' id='LC40'>			<span class="p">}</span></div><div class='line' id='LC41'>			<span class="k">if</span> <span class="p">(</span><span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-maptype&#39;</span><span class="p">)){</span></div><div class='line' id='LC42'>				<span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">zoom</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">attr</span><span class="p">(</span><span class="s1">&#39;data-maptype&#39;</span><span class="p">);</span></div><div class='line' id='LC43'>			<span class="p">}</span></div><div class='line' id='LC44'><br/></div><div class='line' id='LC45'>			<span class="c1">// if there should be more markers _with_ text an ul.markers element should be used so</span></div><div class='line' id='LC46'>			<span class="c1">// we can store all markers :-) (marker specific settings will be added later)</span></div><div class='line' id='LC47'>			<span class="k">if</span> <span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">showMarker</span><span class="p">){</span></div><div class='line' id='LC48'>				<span class="nx">markers</span><span class="p">.</span><span class="nx">push</span><span class="p">({</span></div><div class='line' id='LC49'>					<span class="nx">label</span><span class="o">:</span> <span class="s1">&#39;A&#39;</span><span class="p">,</span></div><div class='line' id='LC50'>					<span class="nx">position</span><span class="o">:</span> <span class="nx">settings</span><span class="p">.</span><span class="nx">center</span></div><div class='line' id='LC51'>				<span class="p">});</span></div><div class='line' id='LC52'>			<span class="p">}</span></div><div class='line' id='LC53'>			<span class="nx">options</span><span class="p">.</span><span class="nx">markers</span> <span class="o">=</span> <span class="nx">markers</span><span class="p">;</span></div><div class='line' id='LC54'>			<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;options&#39;</span><span class="p">,</span> <span class="nx">options</span><span class="p">);</span></div><div class='line' id='LC55'><br/></div><div class='line' id='LC56'>			<span class="k">if</span> <span class="p">(</span><span class="nx">screen</span><span class="p">.</span><span class="nx">width</span> <span class="o">&lt;</span> <span class="nx">options</span><span class="p">.</span><span class="nx">deviceWidth</span><span class="p">){</span></div><div class='line' id='LC57'>				<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">mobileGmap</span><span class="p">(</span><span class="s1">&#39;showImage&#39;</span><span class="p">);</span></div><div class='line' id='LC58'>			<span class="p">}</span><span class="k">else</span><span class="p">{</span></div><div class='line' id='LC59'>				<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">mobileGmap</span><span class="p">(</span><span class="s1">&#39;showMap&#39;</span><span class="p">);</span></div><div class='line' id='LC60'>			<span class="p">}</span></div><div class='line' id='LC61'><br/></div><div class='line' id='LC62'>		<span class="p">},</span></div><div class='line' id='LC63'><br/></div><div class='line' id='LC64'>		<span class="nx">showMap</span> <span class="o">:</span> <span class="kd">function</span><span class="p">(){</span></div><div class='line' id='LC65'>			<span class="kd">var</span> <span class="nx">options</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;options&#39;</span><span class="p">),</span></div><div class='line' id='LC66'>					<span class="nx">geocoder</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">google</span><span class="p">.</span><span class="nx">maps</span><span class="p">.</span><span class="nx">Geocoder</span><span class="p">(),</span></div><div class='line' id='LC67'>					<span class="nx">latlng</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">google</span><span class="p">.</span><span class="nx">maps</span><span class="p">.</span><span class="nx">LatLng</span><span class="p">(</span><span class="o">-</span><span class="mf">34.397</span><span class="p">,</span> <span class="mf">150.644</span><span class="p">),</span></div><div class='line' id='LC68'>					<span class="nx">mapOptions</span> <span class="o">=</span> <span class="p">{},</span></div><div class='line' id='LC69'>					<span class="nx">htmlObj</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">get</span><span class="p">(</span><span class="mi">0</span><span class="p">);</span></div><div class='line' id='LC70'>					<span class="nx">geocoder</span><span class="p">.</span><span class="nx">geocode</span><span class="p">(</span> <span class="p">{</span> <span class="s1">&#39;address&#39;</span><span class="o">:</span> <span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">center</span><span class="p">.</span><span class="nx">replace</span><span class="p">(</span><span class="sr">/\+/gi</span><span class="p">,</span> <span class="s1">&#39; &#39;</span><span class="p">)},</span> <span class="kd">function</span><span class="p">(</span><span class="nx">results</span><span class="p">,</span> <span class="nx">status</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC71'>					      <span class="k">if</span> <span class="p">(</span><span class="nx">status</span> <span class="o">==</span> <span class="nx">google</span><span class="p">.</span><span class="nx">maps</span><span class="p">.</span><span class="nx">GeocoderStatus</span><span class="p">.</span><span class="nx">OK</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC72'>					        <span class="c1">// map.setCenter(results[0].geometry.location);</span></div><div class='line' id='LC73'>					        <span class="nx">mapOptions</span> <span class="o">=</span> <span class="p">{</span></div><div class='line' id='LC74'>										<span class="nx">zoom</span><span class="o">:</span> <span class="nb">parseInt</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">zoom</span><span class="p">,</span> <span class="mi">10</span><span class="p">),</span></div><div class='line' id='LC75'>										<span class="nx">center</span><span class="o">:</span> <span class="nx">results</span><span class="p">[</span><span class="mi">0</span><span class="p">].</span><span class="nx">geometry</span><span class="p">.</span><span class="nx">location</span><span class="p">,</span></div><div class='line' id='LC76'>										<span class="nx">mapTypeId</span><span class="o">:</span> <span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">maptype</span></div><div class='line' id='LC77'>									<span class="p">}</span></div><div class='line' id='LC78'>									<span class="kd">var</span> <span class="nx">map</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">google</span><span class="p">.</span><span class="nx">maps</span><span class="p">.</span><span class="nx">Map</span><span class="p">(</span><span class="nx">htmlObj</span><span class="p">,</span> <span class="nx">mapOptions</span><span class="p">);</span></div><div class='line' id='LC79'>									<span class="kd">var</span> <span class="nx">marker</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">google</span><span class="p">.</span><span class="nx">maps</span><span class="p">.</span><span class="nx">Marker</span><span class="p">({</span></div><div class='line' id='LC80'>									            <span class="nx">map</span><span class="o">:</span> <span class="nx">map</span><span class="p">,</span></div><div class='line' id='LC81'>									            <span class="nx">position</span><span class="o">:</span> <span class="nx">results</span><span class="p">[</span><span class="mi">0</span><span class="p">].</span><span class="nx">geometry</span><span class="p">.</span><span class="nx">location</span></div><div class='line' id='LC82'>									        <span class="p">});</span></div><div class='line' id='LC83'>					      <span class="p">}</span></div><div class='line' id='LC84'>					    <span class="p">});</span></div><div class='line' id='LC85'>		<span class="p">},</span></div><div class='line' id='LC86'><br/></div><div class='line' id='LC87'>		<span class="nx">showImage</span> <span class="o">:</span> <span class="kd">function</span><span class="p">(){</span></div><div class='line' id='LC88'>			<span class="kd">var</span> <span class="nx">par</span> <span class="o">=</span> <span class="p">[],</span></div><div class='line' id='LC89'>					<span class="nx">r</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">Image</span><span class="p">(),</span></div><div class='line' id='LC90'>					<span class="nx">l</span> <span class="o">=</span> <span class="nb">document</span><span class="p">.</span><span class="nx">createElement</span><span class="p">(</span><span class="s1">&#39;a&#39;</span><span class="p">),</span></div><div class='line' id='LC91'>					<span class="nx">options</span> <span class="o">=</span> <span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">data</span><span class="p">(</span><span class="s1">&#39;options&#39;</span><span class="p">),</span></div><div class='line' id='LC92'>					<span class="nx">i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">,</span></div><div class='line' id='LC93'>					<span class="nx">m</span> <span class="o">=</span> <span class="p">[];</span></div><div class='line' id='LC94'>			<span class="k">for</span> <span class="p">(</span><span class="kd">var</span> <span class="nx">o</span> <span class="k">in</span> <span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">){</span></div><div class='line' id='LC95'>				<span class="nx">par</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="nx">o</span> <span class="o">+</span> <span class="s1">&#39;=&#39;</span> <span class="o">+</span> <span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">[</span><span class="nx">o</span><span class="p">]);</span></div><div class='line' id='LC96'>			<span class="p">}</span></div><div class='line' id='LC97'>			<span class="k">if</span> <span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">markers</span><span class="p">.</span><span class="nx">length</span><span class="p">){</span></div><div class='line' id='LC98'>				<span class="kd">var</span> <span class="nx">t</span><span class="o">=</span><span class="p">[];</span></div><div class='line' id='LC99'>				<span class="k">for</span> <span class="p">(;</span><span class="nx">i</span> <span class="o">&lt;</span> <span class="nx">options</span><span class="p">.</span><span class="nx">markers</span><span class="p">.</span><span class="nx">length</span><span class="p">;</span><span class="nx">i</span><span class="o">++</span><span class="p">){</span></div><div class='line' id='LC100'>					<span class="nx">t</span> <span class="o">=</span> <span class="p">[];</span></div><div class='line' id='LC101'>					<span class="k">for</span> <span class="p">(</span><span class="kd">var</span> <span class="nx">j</span> <span class="k">in</span> <span class="nx">options</span><span class="p">.</span><span class="nx">markers</span><span class="p">[</span><span class="nx">i</span><span class="p">]){</span></div><div class='line' id='LC102'>						<span class="k">if</span> <span class="p">(</span><span class="nx">j</span> <span class="o">==</span> <span class="s1">&#39;position&#39;</span><span class="p">){</span></div><div class='line' id='LC103'>							<span class="nx">t</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="nx">options</span><span class="p">.</span><span class="nx">markers</span><span class="p">[</span><span class="nx">i</span><span class="p">][</span><span class="nx">j</span><span class="p">]);</span></div><div class='line' id='LC104'>						<span class="p">}</span><span class="k">else</span><span class="p">{</span></div><div class='line' id='LC105'>							<span class="nx">t</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="nx">j</span> <span class="o">+</span> <span class="s1">&#39;:&#39;</span> <span class="o">+</span> <span class="nx">options</span><span class="p">.</span><span class="nx">markers</span><span class="p">[</span><span class="nx">i</span><span class="p">][</span><span class="nx">j</span><span class="p">]);</span></div><div class='line' id='LC106'>						<span class="p">}</span></div><div class='line' id='LC107'>					<span class="p">}</span></div><div class='line' id='LC108'>					<span class="nx">m</span><span class="p">.</span><span class="nx">push</span><span class="p">(</span><span class="s1">&#39;&amp;markers=&#39;</span> <span class="o">+</span> <span class="nx">t</span><span class="p">.</span><span class="nx">join</span><span class="p">(</span><span class="s1">&#39;%7C&#39;</span><span class="p">));</span></div><div class='line' id='LC109'>				<span class="p">}</span></div><div class='line' id='LC110'>			<span class="p">}</span></div><div class='line' id='LC111'>			<span class="nx">r</span><span class="p">.</span><span class="nx">src</span> <span class="o">=</span>  <span class="nx">options</span><span class="p">.</span><span class="nx">imgURI</span> <span class="o">+</span> <span class="nx">par</span><span class="p">.</span><span class="nx">join</span><span class="p">(</span><span class="s1">&#39;&amp;&#39;</span><span class="p">)</span> <span class="o">+</span> <span class="nx">m</span><span class="p">.</span><span class="nx">join</span><span class="p">(</span><span class="s1">&#39;&#39;</span><span class="p">);</span></div><div class='line' id='LC112'>			<span class="nx">l</span><span class="p">.</span><span class="nx">href</span> <span class="o">=</span> <span class="s1">&#39;http://maps.google.com/maps?q=&#39;</span> <span class="o">+</span> <span class="nx">options</span><span class="p">.</span><span class="nx">settings</span><span class="p">.</span><span class="nx">center</span><span class="p">;</span></div><div class='line' id='LC113'>			<span class="nx">l</span><span class="p">.</span><span class="nx">appendChild</span><span class="p">(</span><span class="nx">r</span><span class="p">);</span></div><div class='line' id='LC114'>			<span class="nx">$</span><span class="p">(</span><span class="k">this</span><span class="p">).</span><span class="nx">empty</span><span class="p">().</span><span class="nx">append</span><span class="p">(</span><span class="nx">l</span><span class="p">);</span></div><div class='line' id='LC115'>		<span class="p">}</span></div><div class='line' id='LC116'><br/></div><div class='line' id='LC117'>	<span class="p">};</span></div><div class='line' id='LC118'><br/></div><div class='line' id='LC119'>	<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">mobileGmap</span> <span class="o">=</span> <span class="kd">function</span><span class="p">(</span><span class="nx">method</span><span class="p">){</span></div><div class='line' id='LC120'>		<span class="k">if</span> <span class="p">(</span> <span class="nx">methods</span><span class="p">[</span><span class="nx">method</span><span class="p">]</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC121'>					<span class="k">return</span> <span class="nx">methods</span><span class="p">[</span> <span class="nx">method</span> <span class="p">].</span><span class="nx">apply</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nb">Array</span><span class="p">.</span><span class="nx">prototype</span><span class="p">.</span><span class="nx">slice</span><span class="p">.</span><span class="nx">call</span><span class="p">(</span> <span class="nx">arguments</span><span class="p">,</span> <span class="mi">1</span> <span class="p">));</span></div><div class='line' id='LC122'>				<span class="p">}</span> <span class="k">else</span> <span class="k">if</span> <span class="p">(</span> <span class="k">typeof</span> <span class="nx">method</span> <span class="o">===</span> <span class="s1">&#39;object&#39;</span> <span class="o">||</span> <span class="o">!</span> <span class="nx">method</span> <span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC123'>					<span class="k">return</span> <span class="nx">methods</span><span class="p">.</span><span class="nx">init</span><span class="p">.</span><span class="nx">apply</span><span class="p">(</span> <span class="k">this</span><span class="p">,</span> <span class="nx">arguments</span> <span class="p">);</span></div><div class='line' id='LC124'>				<span class="p">}</span> <span class="k">else</span> <span class="p">{</span></div><div class='line' id='LC125'>					<span class="nx">$</span><span class="p">.</span><span class="nx">error</span><span class="p">(</span> <span class="s1">&#39;Method &#39;</span> <span class="o">+</span> <span class="nx">method</span> <span class="o">+</span> <span class="s1">&#39; does not exist on jQuery.mobileGmap&#39;</span> <span class="p">);</span></div><div class='line' id='LC126'>		<span class="p">}</span></div><div class='line' id='LC127'>	<span class="p">};</span></div><div class='line' id='LC128'><span class="p">})(</span><span class="k">this</span><span class="p">.</span><span class="nx">jQuery</span><span class="p">);</span></div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>

        <a href="#jump-to-line" rel="facebox" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
        <div id="jump-to-line" style="display:none">
          <h2>Jump to Line</h2>
          <form accept-charset="UTF-8" class="js-jump-to-line-form">
            <input class="textfield js-jump-to-line-field" type="text">
            <div class="full-button">
              <button type="submit" class="button">Go</button>
            </div>
          </form>
        </div>

      </div>
    </div>
</div>

<div id="js-frame-loading-template" class="frame frame-loading large-loading-area" style="display:none;">
  <img class="js-frame-loading-spinner" src="https://a248.e.akamai.net/assets.github.com/images/spinners/octocat-spinner-128.gif?1347543529" height="64" width="64">
</div>


        </div>
      </div>
      <div class="context-overlay"></div>
    </div>

      <div id="footer-push"></div><!-- hack for sticky footer -->
    </div><!-- end of wrapper - hack for sticky footer -->

      <!-- footer -->
      <div id="footer">
  <div class="container clearfix">

      <dl class="footer_nav">
        <dt>GitHub</dt>
        <dd><a href="https://github.com/about">About us</a></dd>
        <dd><a href="https://github.com/blog">Blog</a></dd>
        <dd><a href="https://github.com/contact">Contact &amp; support</a></dd>
        <dd><a href="http://enterprise.github.com/">GitHub Enterprise</a></dd>
        <dd><a href="http://status.github.com/">Site status</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Applications</dt>
        <dd><a href="http://mac.github.com/">GitHub for Mac</a></dd>
        <dd><a href="http://windows.github.com/">GitHub for Windows</a></dd>
        <dd><a href="http://eclipse.github.com/">GitHub for Eclipse</a></dd>
        <dd><a href="http://mobile.github.com/">GitHub mobile apps</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Services</dt>
        <dd><a href="http://get.gaug.es/">Gauges: Web analytics</a></dd>
        <dd><a href="http://speakerdeck.com">Speaker Deck: Presentations</a></dd>
        <dd><a href="https://gist.github.com">Gist: Code snippets</a></dd>
        <dd><a href="http://jobs.github.com/">Job board</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Documentation</dt>
        <dd><a href="http://help.github.com/">GitHub Help</a></dd>
        <dd><a href="http://developer.github.com/">Developer API</a></dd>
        <dd><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></dd>
        <dd><a href="http://pages.github.com/">GitHub Pages</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>More</dt>
        <dd><a href="http://training.github.com/">Training</a></dd>
        <dd><a href="https://github.com/edu">Students &amp; teachers</a></dd>
        <dd><a href="http://shop.github.com">The Shop</a></dd>
        <dd><a href="/plans">Plans &amp; pricing</a></dd>
        <dd><a href="http://octodex.github.com/">The Octodex</a></dd>
      </dl>

      <hr class="footer-divider">


    <p class="right">&copy; 2013 <span title="0.06560s from fe13.rs.github.com">GitHub</span>, Inc. All rights reserved.</p>
    <a class="left" href="https://github.com/">
      <span class="mega-icon mega-icon-invertocat"></span>
    </a>
    <ul id="legal">
        <li><a href="https://github.com/site/terms">Terms of Service</a></li>
        <li><a href="https://github.com/site/privacy">Privacy</a></li>
        <li><a href="https://github.com/security">Security</a></li>
    </ul>

  </div><!-- /.container -->

</div><!-- /.#footer -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/joggink/mobileGmap/suggestions/commit">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-icon mega-icon-normalscreen"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="mini-icon mini-icon-brightness"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

    
    
    <span id='server_response_time' data-time='0.06612' data-host='fe13'></span>
    
  </body>
</html>

