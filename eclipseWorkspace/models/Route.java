import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/*
 * Route models a real world bus route.
 * Includes all the bus stops.
 */
public class Route {

	private String routeID;
	private String agencyID;
	private String routeShortName;
	private String routeLongName;
	private String routeType;
	private ArrayList<BusStop> busStops;

	// Constructor
	public Route(String routeID, String agencyID, String routeShortName, String routeLongName, String routeType) {
		this.routeID = routeID;
		this.agencyID = agencyID;
		this.routeShortName = routeShortName;
		this.routeLongName = routeLongName;
		this.routeType = routeType;
		
		this.busStops = new ArrayList<BusStop>();
	}// Roster constructor

	@Override
	public String toString()
	{
		return "RouteID: " + this.routeID +
			   " AgencyID: " + this.agencyID + 
			   " RouteShortName: " + this.routeShortName + 
			   " RouteLongName: " + this.routeLongName + 
			   " RoutType: " + this.routeType;
	}
	
	private void getBusStops(){
		
	}
	
}// class Route
