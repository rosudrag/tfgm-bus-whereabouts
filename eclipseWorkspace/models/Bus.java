import java.net.URI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class Bus {

	private String route;
	private String[] busStops;
	private String nextBusStop;
	private double currentSpeed;
	private double distanceToNextStop;
	private String[] keyLocationNearby;
	private String[] links;

	// constructor
	public Bus(String route) {
		this.route = route;
	}// Bus constructor
	
	

	private static URI getBaseURI() {
		return UriBuilder.fromUri(
				"http://localhost:8080/de.vogella.jersey.first").build();
	}
}// Bus class
