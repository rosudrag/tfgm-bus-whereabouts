public class BusStop {
	private String name;
	private String atcoCode;
	private String lat;
	private String lon;
	private String stopType;
	private String stopSubType;
	private String timingStatus;

	/*
	 * "AtcoCode": "1800EB01341", "CommonName": "Piccadilly/Oldham St",
	 * "Latitude": 53.4819354870976, "Longitude": -2.2363850781166, "StopType":
	 * "BCT", "StopSubType": "MKD", "TimingStatus": "TIP"
	 */

	public BusStop(String atcoCode, String name, String lat, String lon,
			String stopType, String stopSubType, String timingStatus) {
		this.name = name;
		this.atcoCode = atcoCode;
		this.lat = lat;
		this.lon = lon;
		this.stopType = stopType;
		this.stopSubType = stopSubType;
		this.timingStatus = timingStatus;
	}

	public String getName() {
		return this.name;
	}
}
