package controllers;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import models.Bus;
import models.ReadRoutes;
import play.*;
import play.libs.Akka;
import play.libs.Comet;
import play.libs.F.Callback0;
import play.mvc.*;
import scala.concurrent.duration.Duration;

import views.html.*;
import models.Global;

/*
 * The main App is a simulation of a Bus holding a webapp to which anyone can connect.
 * 
 */
public class Application extends Controller {
	private static int timer;
	private static Bus theBus = null;
	private static int tickTime = 6500;

	public static Result index() {
		if (theBus == null) {
			ReadRoutes.takeInfo();
			theBus = new Bus(ReadRoutes.myRoutes.get(15));
		}

		timer = 0;
		return ok(index.render(theBus));
	}

	public static Result test() {
		if (theBus == null) {
			ReadRoutes.takeInfo();
			theBus = new Bus(ReadRoutes.myRoutes.get(15));
		}

		timer = 0;
		return ok(test.render(theBus));
	}

	// STUFF FROM CLOCK COMET
	final static ActorRef clock = Clock.instance;

	public static Result liveClock() {
		return ok(new Comet("parent.clockChanged") {
			public void onConnected() {
				clock.tell(this);
			}
		});
	}

	public static class Clock extends UntypedActor {

		static ActorRef instance = Akka.system()
				.actorOf(new Props(Clock.class));

		// Send a TICK message every 100 millis
		static {
			Akka.system()
					.scheduler()
					.schedule(Duration.Zero(),
							Duration.create(tickTime, MILLISECONDS), instance,
							"TICK", Akka.system().dispatcher());
		}

		//

		List<Comet> sockets = new ArrayList<Comet>();

		public void onReceive(Object message) {

			// Handle connections
			if (message instanceof Comet) {
				final Comet cometSocket = (Comet) message;

				if (sockets.contains(cometSocket)) {

					// Brower is disconnected
					sockets.remove(cometSocket);
					Logger.info("Browser disconnected (" + sockets.size()
							+ " browsers currently connected)");

				} else {

					// Register disconnected callback
					cometSocket.onDisconnected(new Callback0() {
						public void invoke() {
							getContext().self().tell(cometSocket);
						}
					});

					// New browser connected
					sockets.add(cometSocket);
					Logger.info("New browser connected (" + sockets.size()
							+ " browsers currently connected)");

				}

			}

			// Tick, send time to all connected browsers
			if ("TICK".equals(message)) {
				String nextBusStop = theBus.getMessage(0);
				String currentRoute = theBus.getRoute();
				String keyLoc1;
				String keyLoc2;
				String keyLoc3;
				String stopsLeft = "" + theBus.getStopsLeft();
				String distance = theBus.getDistanceToNextStop() + " metres.";
				String duration = theBus.getDurationToNextStop() + " seconds.";

				if (Global.usePlaces) {
					keyLoc1 = theBus.getMessage(1);
					keyLoc2 = theBus.getMessage(2);
					keyLoc3 = theBus.getMessage(3);
				}

				// Send the current time to all comet sockets
				for (Comet cometSocket : sockets) {
					cometSocket.sendMessage(nextBusStop);
					cometSocket.sendMessage(currentRoute);
					cometSocket.sendMessage(stopsLeft);
					cometSocket.sendMessage(distance);
					cometSocket.sendMessage(duration);

					if (Global.usePlaces) {
						cometSocket.sendMessage(keyLoc1);
						cometSocket.sendMessage(keyLoc2);
						cometSocket.sendMessage(keyLoc3);
					}
				}
				timer += tickTime;
				theBus.incrementNextBusStop();
				theBus.incrementCurrentBusStop();
			}

		}

	}
}
