package models;

import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;

import play.libs.WS;
import play.libs.WS.Response;


/*
 * A bus stop is modeled from TfGM real time dataset. 
 * Getting nearby key locations is done using google api using local lon and lat.
 * Google Api restricts searches to only 1k searches / day.
 */
public class BusStop {
	private String name;
	private String atcoCode;
	private String lat;
	private String lon;
	private String stopType;
	private String stopSubType;
	private String timingStatus;
	private int order;

	private ArrayList<String> keyLocations;

	/*
	 * "AtcoCode": "1800EB01341", "CommonName": "Piccadilly/Oldham St",
	 * "Latitude": 53.4819354870976, "Longitude": -2.2363850781166, "StopType":
	 * "BCT", "StopSubType": "MKD", "TimingStatus": "TIP"
	 */

	public BusStop(String atcoCode, String name, String lat, String lon,
			String stopType, String stopSubType, String timingStatus, int order) {
		this.name = name;
		this.atcoCode = atcoCode;
		this.lat = lat;
		this.lon = lon;
		this.stopType = stopType;
		this.stopSubType = stopSubType;
		this.timingStatus = timingStatus;

		this.order = order;
	}

	public String getName() {
		return this.name;
	}

	// Use google api to retrieve nearby key locations
	/*
	 * Example:
	 * https://maps.googleapis.com/maps/api/place/nearbysearch/output?key
	 * =AIzaSyAgYhBvquzsGB2WkTGlCYbBlVtt05U8wg0
	 * &location=53.4819354870976,-2.2363850781166&radius=250&sensor=false
	 */
	public void populateKeyLocations() {
		ArrayList<String> locations = new ArrayList<String>();

		String googleKey = Global.googleKey;
		String googlePlacesUri = Global.googlePlacesUri;

		String location = "" + this.lat + "," + this.lon;
		String radius = "250";
		String sensor = "false";
		String rankby = "prominence";

		googlePlacesUri += "key=" + googleKey + "&";
		googlePlacesUri += "location=" + location + "&";
		googlePlacesUri += "radius=" + radius + "&";
		googlePlacesUri += "sensor=" + sensor + "&";
		googlePlacesUri += "rankby=" + rankby + "&";

		Response response = WS.url(googlePlacesUri)
				.setQueryParameter("key", googleKey)
				.setQueryParameter("location", location)
				.setQueryParameter("radius", radius)
				.setQueryParameter("sensor", sensor)
				.setQueryParameter("rankby", rankby).get().get();

		JsonNode nodes = response.asJson().findValue("results");

		//Skip first place
		int i = 0;
		for (JsonNode node : nodes) {
			if (i == 0) {
				i++;
				continue;
			}
			String name;

			name = node.findValue("name").asText();
			locations.add(name);

			i++;
			if (i > 4)
				break;

		}
		this.keyLocations = locations;
	}

	// Debugging method - change to private when done
	public ArrayList<String> getKeyLocations() {
		if(Global.usePlaces)
			this.populateKeyLocations();
		return this.keyLocations;
	}

	// The index in the arraylist it belongs to. Easier to sort and search.
	public int getOrder() {
		return this.order;
	}
	
	public String getLat(){
		return this.lat;
	}
	
	public String getLon(){
		return this.lon;
	}

	@Override
	public String toString() {
		return this.name + " " + this.atcoCode + " " + this.lat + " "
				+ this.lon + " " + this.stopType + " " + this.stopSubType + " "
				+ this.timingStatus + " " + this.order;
	}

}
