package models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class readRoutes {
	
	public static ArrayList<BusRoute> myRoutes = new ArrayList<BusRoute>();

	//Method to read the file for taking the routes for buses.
	public static void takeInfo() {
		BufferedReader file = null;

		try {
			String currentLine;
			String routeID;
			String agencyID;
			String routeShortName;
			String routeLongName;
			String routeType;

			file = new BufferedReader(new FileReader("data/gtdf-out/routes.txt"));
			
			currentLine = file.readLine();
			while ((currentLine = file.readLine()) != null) {
				//System.out.println(currentLine);
				String[] values = currentLine.split(",");
				
				//For creating new route
				routeID = values[0];
				agencyID = values[1];
				routeShortName = values[2];
				routeLongName = values[3];
				routeType = values[4];
				
				//Add route to myRoutes only if it's a bus route.
				if (routeType.equals("3"))
					myRoutes.add(new BusRoute(routeID, agencyID, routeShortName, routeLongName, routeType));
			}// while

			for(int i = 0; i < myRoutes.size(); i++) {   
			    System.out.println(myRoutes.get(i));
			}
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				if (file != null)
					file.close();
			} catch (IOException e) {
				System.out.println(e);
			}// catch
		}// finally

	}// takeInfo
	
	
	
}//readRoutes class
