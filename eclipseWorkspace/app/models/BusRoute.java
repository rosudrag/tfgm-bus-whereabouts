package models;

import java.util.ArrayList;
import java.util.Collections;

import org.codehaus.jackson.JsonNode;
import play.libs.WS;
import play.libs.WS.Response;

import models.Global;

/*
 * Route models a real world bus route.
 * Includes all the bus stops.
 */
public class BusRoute {

	public enum TYPE {
		IN, OUT, CIRCULAR
	};

	private String routeID;
	private String agencyID; // GMS: 111:I:
	private String routeShortName;
	private String routeLongName;
	private String routeType; // Bus , metro
	private TYPE type; // In, out, Circular

	// Constructor
	public BusRoute(String routeID, String agencyID, String routeShortName,
			String routeLongName, String routeType) {
		this.routeID = routeID;
		this.agencyID = agencyID;
		this.routeShortName = routeShortName;
		this.routeLongName = routeLongName;
		this.routeType = routeType;
		this.setType();
	}// Roster constructor

	@Override
	public String toString() {
		return "RouteID: " + this.routeID + " AgencyID: " + this.agencyID
				+ " RouteShortName: " + this.routeShortName
				+ " RouteLongName: " + this.routeLongName + " RoutType: "
				+ this.routeType;
	}

	private void setType(){
//		System.out.println(this.agencyID);
//		String[] values = (this.agencyID.replaceAll("\\s","")).split(":");
//		System.out.println(values[1]);
//		String result = values[1];
//		
//		if(result.contains("I"))
//			this.type = TYPE.IN;
//		else if(result.contains("O"))
//			this.type = TYPE.OUT;
//		else if(result.contains("C"))
//			this.type = TYPE.CIRCULAR;
	}

	/*
	 * Get all the bus stops from a certain GM route
	 */
	public ArrayList<BusStop> getBusStops() {
		ArrayList<BusStop> stops = new ArrayList<BusStop>();

		// Url ex http://opendata.tfgm.com/api/routes/x84/stops
		String url = "http://opendata.tfgm.com/api/routes/";
		url += this.routeShortName.replaceAll("\\s", "") + "/stops";

		String headerType1 = "Accept";
		String headerType2 = "DevKey";
		String headerType3 = "AppKey";

		String header1 = "text/json";
		String header2 = Global.TfGMDevKey;
		String header3 = Global.TfGMAppKey;

		Response response = WS.url(url).setHeader(headerType1, header1)
				.setHeader(headerType2, header2)
				.setHeader(headerType3, header3).get().get();

		JsonNode nodes = response.asJson();

		int order = 0;
		for (JsonNode node : nodes) {
			String name;
			String atcoCode;
			String lat;
			String lon;
			String stopType;
			String stopSubType;
			String timingStatus;

			atcoCode = node.findValue("AtcoCode").asText();
			name = node.findValue("CommonName").asText();
			lat = node.findValue("Latitude").asText();
			lon = node.findValue("Longitude").asText();
			stopType = node.findValue("StopType").asText();
			stopSubType = node.findValue("StopSubType").asText();
			timingStatus = node.findValue("TimingStatus").asText();

			BusStop stop = new BusStop(atcoCode, name, lat, lon, stopType,
					stopSubType, timingStatus, order);

			order++;
			stops.add(stop);
		}
		
		// Reverse the order of the bus stops if route is opposite
		if(this.type == TYPE.OUT)
			Collections.reverse(stops);
		return stops;
	}

	public String getRouteShortName() {
		return this.routeShortName;
	}

}// class Route
