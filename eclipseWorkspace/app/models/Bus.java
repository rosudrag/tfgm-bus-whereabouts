package models;

import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;

import play.libs.WS;
import play.libs.WS.Response;

/*
 * A valid bus is an object that has a valid route from GM.
 * 
 * It should model certain information about the route and the bus while it is on it's journey:
 * such as bus stops, how far untill next bus stop etc.
 */
public class Bus {

	private BusRoute route;
	private ArrayList<BusStop> busStops;
	private BusStop nextBusStop;
	private BusStop currentBusStop;
	public ArrayList<String> screenMsg;

	private String duration;
	private String distance;

	private int stopsLeft;
	private int nrStops;

	private ArrayList<String> latList;
	private ArrayList<String> lonList;

	// Takes the route as input.
	// ASSUMES it starts at BUS STOP 0 (Need to reverse the bus stops arraylist
	// otherwise)
	public Bus(BusRoute route) {
		this.route = route;
		this.busStops = this.route.getBusStops();

		this.currentBusStop = this.busStops.get(0);
		this.nextBusStop = this.busStops.get(0);

		this.nrStops = this.busStops.size();
		this.stopsLeft = this.nrStops;

		populateLatLonList();
		incrementNextBusStop();
	}// Bus constructor

	/*
	 * Updates the next bus stop by +1 Returns False if only if next stop is
	 * final. Also updates the messages displayed on screen.
	 */
	@SuppressWarnings("unused")
	public boolean incrementNextBusStop() {
		// Next stop == last stop
		int nextStopNumber = 1 + this.nextBusStop.getOrder();
		if (nextStopNumber >= busStops.size())
			return false;

		this.nextBusStop = this.busStops.get(nextStopNumber);
		this.screenMsg = updateScreenMsg();
		this.stopsLeft--;
		this.findStatisticsBetween(this.currentBusStop, this.nextBusStop);
		return true;
	}

	// Updates current bus stop to next bus stop
	public boolean incrementCurrentBusStop() {
		this.currentBusStop = this.nextBusStop;
		return true;
	}

	private ArrayList<String> updateScreenMsg() {
		ArrayList<String> msgs = new ArrayList<String>();

		msgs.add("Next Stop: " + this.nextBusStop.getName());

		if (Global.usePlaces) {
			ArrayList<String> keyLocations = this.nextBusStop.getKeyLocations();
			msgs.addAll(1, keyLocations);
		}

		return msgs;
	}

	@Override
	public String toString() {
		String result = "";

		result += "Bus route " + this.route + "\n";
		for (String string : this.screenMsg) {
			result += string + "\n";
		}

		return result;
	}

	/*
	 * Gets the distance between two bus stops using the google dinstance api.
	 * Stores the statistics in an ArrayList<Integer>
	 */
	private void findStatisticsBetween(BusStop a, BusStop b) {
		int distance = 0;
		int duration = 0;

		String googleKey = Global.googleKey;
		String googleDistanceUri = Global.googleDistanceUri;
		String origin = "" + a.getLat() + "," + a.getLon();
		String destination = "" + b.getLat() + "," + b.getLon();
		String sensor = "false";
		String units = "metric";

		Response response = WS.url(googleDistanceUri)
				.setQueryParameter("origin", origin)
				.setQueryParameter("destination", destination)
				.setQueryParameter("sensor", sensor)
				.setQueryParameter("units", units).get().get();

		JsonNode nodes = response.asJson();

		for (JsonNode nodeLegs : nodes.findValues("routes")) { // for each Leg
			for (JsonNode nodeStep : nodeLegs.findValue("steps")) {
				distance += nodeStep.findValue("distance").findValue("value")
						.asInt();
				duration += nodeStep.findValue("duration").findValue("value")
						.asInt();
			}
		}
		this.distance = "" + distance;
		this.duration = "" + duration;
	}

	private void populateLatLonList() {
		ArrayList<String> lats = new ArrayList<String>();
		ArrayList<String> lons = new ArrayList<String>();
		for (BusStop stop : this.busStops) {
			lats.add(stop.getLat());
			lons.add(stop.getLon());
		}

		this.latList = lats;
		this.lonList = lons;
	}

	public String getMessage(int index) {
		return this.screenMsg.get(index);
	}

	public String getRoute() {
		return this.route.getRouteShortName();
	}

	public int getNrStops() {
		return this.nrStops;
	}

	public int getStopsLeft() {
		return this.stopsLeft;
	}

	public String getDurationToNextStop() {
		return this.duration;
	}

	public String getDistanceToNextStop() {
		return this.distance;
	}

	public String getCurrentStopLat() {
		return this.currentBusStop.getLat();
	}

	public String getCurrentStopLon() {
		return this.currentBusStop.getLon();
	}

	public String getNextStopLat() {
		return this.nextBusStop.getLat();
	}

	public String getNextStopLon() {
		return this.nextBusStop.getLon();
	}

	public String getLon(int index) {
		return this.lonList.get(index);
	}

	public String getLat(int index) {
		return this.latList.get(index);
	}
}// Bus class
